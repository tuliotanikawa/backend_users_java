package br.com.tanikawatulio.exemplo.service;

import java.util.List;

import br.com.tanikawatulio.exemplo.service.dto.UserDTO;

public interface UserService {
	
	public UserDTO save(UserDTO userDTO);

	public UserDTO update(Long id, UserDTO userDTO);
	
	public List<UserDTO> filterUsers(UserDTO userDTO);

	public UserDTO findOne(Long id);
	
	public void delete(Long id);
		
}
