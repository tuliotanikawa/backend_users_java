package br.com.tanikawatulio.exemplo.service.dto;

import java.time.ZonedDateTime;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class UserDTO {
	@Id
	private Long id;

	private String username;

	private String password;
	
	private String name;
	
	private boolean is_enabled;

	private String surname;
	
	private String email;
	
	private String phone;

	private ZonedDateTime register_date;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isIs_enabled() {
		return is_enabled;
	}

	public void setIs_enabled(boolean is_enabled) {
		this.is_enabled = is_enabled;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public ZonedDateTime getRegister_date() {
		return register_date;
	}

	public void setRegister_date(ZonedDateTime register_date) {
		this.register_date = register_date;
	}
}
