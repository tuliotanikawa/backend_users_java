package br.com.tanikawatulio.exemplo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.tanikawatulio.exemplo.domain.User;
import br.com.tanikawatulio.exemplo.exception.ResourceNotFoundException;
import br.com.tanikawatulio.exemplo.repository.UserRepository;
import br.com.tanikawatulio.exemplo.service.UserService;
import br.com.tanikawatulio.exemplo.service.dto.UserDTO;
import br.com.tanikawatulio.exemplo.service.mapper.UserMapper;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepository;

	@Override
	public UserDTO save(UserDTO userDTO) {
		//I know we do not need to hash or authenticate anything in this technical challenge, but it's a nice practice to follow. 
		userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt()));
		return UserMapper.convertToUserDTO(userRepository.save(UserMapper.convertToUser(userDTO)));
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserDTO> filterUsers(UserDTO userDTO) {
		if (userDTO != null) {
			ExampleMatcher matcher = ExampleMatcher.matchingAll().withIgnoreNullValues().withIgnorePaths("id", "password", "is_enabled", "surname", "phone", "register_date");
			return userRepository.findAll(Example.of(UserMapper.convertToUser(userDTO), matcher)).stream().map(user -> UserMapper.convertToUserDTO(user)).collect(Collectors.toList());
		} else {
			return userRepository.findAll().stream().map(user -> UserMapper.convertToUserDTO(user)).collect(Collectors.toList());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public UserDTO findOne(Long id) {
		User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
		return UserMapper.convertToUserDTO(user);
	}

	@Override
	public UserDTO update(Long id, UserDTO userDTO) {
		//I know we do not need to hash or authenticate anything in this technical challenge, but it's a nice practice to follow. 
		userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt()));
		User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
		user.setUsername(userDTO.getUsername());
		user.setName(userDTO.getName());
		user.setSurname(userDTO.getSurname());
		user.setEmail(userDTO.getEmail());
		user.setPhone(userDTO.getPhone());
		user.setPassword(userDTO.getPassword());
		return UserMapper.convertToUserDTO(user);
	}

	@Override
	public void delete(Long id) {
		userRepository.deleteById(id);
	}

}
