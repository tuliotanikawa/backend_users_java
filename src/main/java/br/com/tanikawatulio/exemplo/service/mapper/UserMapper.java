package br.com.tanikawatulio.exemplo.service.mapper;

import br.com.tanikawatulio.exemplo.domain.User;
import br.com.tanikawatulio.exemplo.service.dto.UserDTO;

public class UserMapper {
	
	public static User convertToUser(UserDTO userDTO) {
		User user = new User();
		user.setId(userDTO.getId());
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setName(userDTO.getName());
		user.setSurname(userDTO.getSurname());
		user.setEmail(userDTO.getEmail());
		user.setPhone(userDTO.getPhone());
		return user;
	}

	public static UserDTO convertToUserDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setUsername(user.getUsername());
		userDTO.setPassword(user.getPassword());
		userDTO.setName(user.getName());
		userDTO.setIs_enabled(user.isIs_enabled());
		userDTO.setSurname(user.getSurname());
		userDTO.setEmail(user.getEmail());
		userDTO.setPhone(user.getPhone());
		userDTO.setRegister_date(user.getRegister_date());
		return userDTO;
	}

}
