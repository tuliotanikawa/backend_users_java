package br.com.tanikawatulio.exemplo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
   
	private static final long serialVersionUID = 4259062600628044875L;
	private String apiResourceName;
    private String resourceFieldName;
    private Object resourceFieldValue;

    public ResourceNotFoundException( String apiResourceName, String resourceFieldName, Object resourceFieldValue) {
        super(String.format("%s was not found searching by field %s : '%s'", apiResourceName, resourceFieldName, resourceFieldValue));
        this.apiResourceName = apiResourceName;
        this.resourceFieldName = resourceFieldName;
        this.resourceFieldValue = resourceFieldValue;
    }

    public String getApiResourceName() {
        return apiResourceName;
    }

    public String getResourceFieldName() {
        return resourceFieldName;
    }

    public Object getResourceFieldValue() {
        return resourceFieldValue;
    }
}

