package br.com.tanikawatulio.exemplo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tanikawatulio.exemplo.domain.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
}
