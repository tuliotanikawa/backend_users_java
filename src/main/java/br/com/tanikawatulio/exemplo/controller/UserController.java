package br.com.tanikawatulio.exemplo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tanikawatulio.exemplo.service.dto.UserDTO;
import br.com.tanikawatulio.exemplo.service.impl.UserServiceImpl;

@RestController
@CrossOrigin
@RequestMapping("/api/users")
public class UserController {

	@Autowired
	UserServiceImpl userServiceImpl;

//	 @Valid is very handy for so called validation cascading when you want
//	 to validate a complex graph and not just a top-level elements of an object.
//	 Every time you want to go deeper, you have to use @Valid
	@PostMapping
	public UserDTO save(@Valid @RequestBody UserDTO userDTO) {
		return userServiceImpl.save(userDTO);
	}

	@GetMapping
	public List<UserDTO> filterUsers(UserDTO userDTO) {
		return userServiceImpl.filterUsers(userDTO);
	}

	@GetMapping("/{id}")
	public UserDTO getUserById(@PathVariable(value = "id") Long userId) {
		return userServiceImpl.findOne(userId);
	}

	@PutMapping("/{id}")
	public UserDTO updateUser(@PathVariable(value = "id") Long userId, @Valid @RequestBody UserDTO userDTO) {
		return userServiceImpl.update(userId, userDTO);
	}

	@DeleteMapping("/{id}")
	public void deleteUserById(@PathVariable(value = "id") Long userId) {
		userServiceImpl.delete(userId);
	}

}
