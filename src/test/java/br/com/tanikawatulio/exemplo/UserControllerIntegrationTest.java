package br.com.tanikawatulio.exemplo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.tanikawatulio.exemplo.service.dto.UserDTO;

@RunWith(SpringJUnit4ClassRunner.class)
//RANDOM_PORT means that your server will start on a port that's allocated by the 
//operating system and is guaranteed to be available. Spring Boot makes this port 
//available to your application by using the @LocalServerPort annotation on a field 
//in your test class. It also auto-configures the default TestRestTemplate instance to use this port.
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class UserControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
    private EntityManagerFactory entityManagerFactory;

    HttpHeaders headers = new HttpHeaders();

    @Before
    public void setUp() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.createNativeQuery("DELETE FROM users").executeUpdate();
        em.getTransaction().commit();
    }

	@Test
	public void shouldAddUser() {
		//setup 1 user
		UserDTO newUser = new UserDTO();
		newUser.setName("tulio");
		newUser.setEmail("tuliotanikawa@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("teste");
		newUser.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);

		ResponseEntity<UserDTO> response = restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);

		UserDTO userDTOAdded = response.getBody();

		assertEquals(newUser.getName(), userDTOAdded.getName());
		assertEquals(newUser.getUsername(), userDTOAdded.getUsername());
		assertEquals(newUser.getEmail(), userDTOAdded.getEmail());
	}

	@Test
	public void shouldAddUsersAndFindThemAll() {
		// Setup 3 users to find them all by searching without a filter
		// users
		UserDTO newUser = new UserDTO();
		newUser.setName("UserOne");
		newUser.setEmail("sameEmail@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("usernameOne");
		newUser.setIs_enabled(true);

		UserDTO newUser2 = new UserDTO();
		newUser2.setName("UserTwo");
		newUser2.setEmail("sameEmail@gmail.com");
		newUser2.setPassword("456");
		newUser2.setPhone("1234578");
		newUser2.setSurname("cesar");
		newUser2.setUsername("usernameTwo");
		newUser2.setIs_enabled(true);

		UserDTO newUser3 = new UserDTO();
		newUser3.setName("userThree");
		newUser3.setEmail("johndoe@hotmail.com");
		newUser3.setPassword("456456");
		newUser3.setPhone("6346373456");
		newUser3.setSurname("doe");
		newUser3.setUsername("differentGuy");
		newUser3.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);
		HttpEntity<UserDTO> entity2 = new HttpEntity<UserDTO>(newUser2, headers);
		HttpEntity<UserDTO> entity3 = new HttpEntity<UserDTO>(newUser3, headers);

		restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity2, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity3, UserDTO.class);

		// Find all users

		String url = "/api/users/";
		ResponseEntity<List> responseFromFindAll = restTemplate.getForEntity(url, List.class);

		List list = responseFromFindAll.getBody();

		assertEquals(HttpStatus.OK, responseFromFindAll.getStatusCode());
		assertEquals(3, list.size());
	}

	@Test
	public void shouldAddAndFindUser() {
		// Setup
		UserDTO newUser = new UserDTO();
		newUser.setName("tulio");
		newUser.setEmail("tuliotanikawa@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("teste");
		newUser.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);

		ResponseEntity<UserDTO> response = restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);

		UserDTO userDTOAdded = response.getBody();

		// FindByID
		String url = String.format("/api/users/%d", userDTOAdded.getId());
		ResponseEntity<UserDTO> responseFromFindById = restTemplate.getForEntity(url, UserDTO.class);

		UserDTO userDTO = response.getBody();
		assertEquals(HttpStatus.OK, responseFromFindById.getStatusCode());
		assertEquals(userDTOAdded.getName(), userDTO.getName());
		assertEquals(userDTOAdded.getSurname(), userDTO.getSurname());
		assertEquals(userDTOAdded.getEmail(), userDTO.getEmail());
	}

	@Test
	public void shouldAddUsersAndFindThemByName() {
		// Setup 2 users with same first name for the sake of bringing a list of 2 users, and a third user with different name
		UserDTO newUser = new UserDTO();
		newUser.setName("sameName");
		newUser.setEmail("tuliotanikawa@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("firstTulio");
		newUser.setIs_enabled(true);

		UserDTO newUser2 = new UserDTO();
		newUser2.setName("sameName");
		newUser2.setEmail("tuliocesar@hotmail.com");
		newUser2.setPassword("456");
		newUser2.setPhone("1234578");
		newUser2.setSurname("cesar");
		newUser2.setUsername("secondTulio");
		newUser2.setIs_enabled(true);

		UserDTO newUser3 = new UserDTO();
		newUser3.setName("differentName");
		newUser3.setEmail("johndoe@hotmail.com");
		newUser3.setPassword("456456");
		newUser3.setPhone("6346373456");
		newUser3.setSurname("doe");
		newUser3.setUsername("differentGuy");
		newUser3.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);
		HttpEntity<UserDTO> entity2 = new HttpEntity<UserDTO>(newUser2, headers);
		HttpEntity<UserDTO> entity3 = new HttpEntity<UserDTO>(newUser3, headers);

		ResponseEntity<UserDTO> response = restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity2, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity3, UserDTO.class);

		UserDTO userDTOAdded = response.getBody();

		// FindByName

		String url = String.format("/api/users/?name=%s", userDTOAdded.getName());
		ResponseEntity<String> responseFromFindByName = restTemplate.getForEntity(url, String.class);

		String body = responseFromFindByName.getBody();
		int usersFoundSize = 0;
		Iterator<JsonNode> usersfoundIterator;
		try {
			usersfoundIterator = new ObjectMapper().readTree(body).iterator();
			while (usersfoundIterator.hasNext()) {
				usersFoundSize++;
				JsonNode usersFound = usersfoundIterator.next();
				assertEquals(userDTOAdded.getName(), usersFound.get("name").textValue());
			}
		} catch (IOException e) {
			fail("error getting list of drivers from json");
		}

		assertEquals(HttpStatus.OK, responseFromFindByName.getStatusCode());
		assertEquals(2, usersFoundSize);

	}

	@Test
	public void shouldAddUsersAndFindThemByUsername() {
		// Setup 2 users with same username for the sake of bringing a list of 2 users and a third user with different a username
		UserDTO newUser = new UserDTO();
		newUser.setName("UserOne");
		newUser.setEmail("userOne@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("TulioUserName");
		newUser.setIs_enabled(true);

		UserDTO newUser2 = new UserDTO();
		newUser2.setName("UserTwo");
		newUser2.setEmail("userTwo@hotmail.com");
		newUser2.setPassword("456");
		newUser2.setPhone("1234578");
		newUser2.setSurname("cesar");
		newUser2.setUsername("TulioUserName");
		newUser2.setIs_enabled(true);

		UserDTO newUser3 = new UserDTO();
		newUser3.setName("UserThree");
		newUser3.setEmail("johndoe@hotmail.com");
		newUser3.setPassword("456456");
		newUser3.setPhone("6346373456");
		newUser3.setSurname("doe");
		newUser3.setUsername("differentGuy");
		newUser3.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);
		HttpEntity<UserDTO> entity2 = new HttpEntity<UserDTO>(newUser2, headers);
		HttpEntity<UserDTO> entity3 = new HttpEntity<UserDTO>(newUser3, headers);

		ResponseEntity<UserDTO> response = restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity2, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity3, UserDTO.class);

		UserDTO userDTOAdded = response.getBody();

		// FindByUserName

		String url = String.format("/api/users/?username=%s", userDTOAdded.getUsername());
		ResponseEntity<String> responseFromFindByUsername = restTemplate.getForEntity(url, String.class);

		String body = responseFromFindByUsername.getBody();
		int usersFoundSize = 0;
		Iterator<JsonNode> usersfoundIterator;
		try {
			usersfoundIterator = new ObjectMapper().readTree(body).iterator();
			while (usersfoundIterator.hasNext()) {
				usersFoundSize++;
				JsonNode usersFound = usersfoundIterator.next();
				assertEquals(userDTOAdded.getUsername(), usersFound.get("username").textValue());
			}
		} catch (IOException e) {
			fail("error getting list of drivers from json");
		}

		assertEquals(2, usersFoundSize);
		assertEquals(HttpStatus.OK, responseFromFindByUsername.getStatusCode());

	}

	@Test
	public void shouldAddUsersAndFindThemByEmail() {
		// Setup 2 users with same email accounts for the sake of bringing a list of 2 and a third user with a different email
		// users
		UserDTO newUser = new UserDTO();
		newUser.setName("UserOne");
		newUser.setEmail("sameEmail@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("usernameOne");
		newUser.setIs_enabled(true);

		UserDTO newUser2 = new UserDTO();
		newUser2.setName("UserTwo");
		newUser2.setEmail("sameEmail@gmail.com");
		newUser2.setPassword("456");
		newUser2.setPhone("1234578");
		newUser2.setSurname("cesar");
		newUser2.setUsername("usernameTwo");
		newUser2.setIs_enabled(true);

		UserDTO newUser3 = new UserDTO();
		newUser3.setName("differentName");
		newUser3.setEmail("johndoe@hotmail.com");
		newUser3.setPassword("456456");
		newUser3.setPhone("6346373456");
		newUser3.setSurname("doe");
		newUser3.setUsername("differentGuy");
		newUser3.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);
		HttpEntity<UserDTO> entity2 = new HttpEntity<UserDTO>(newUser2, headers);
		HttpEntity<UserDTO> entity3 = new HttpEntity<UserDTO>(newUser3, headers);

		ResponseEntity<UserDTO> response = restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity2, UserDTO.class);
		restTemplate.exchange("/api/users/", HttpMethod.POST, entity3, UserDTO.class);

		UserDTO userDTOAdded = response.getBody();

		// FindByEmail

		String url = String.format("/api/users/?email=%s", userDTOAdded.getEmail());
		ResponseEntity<String> responseFromFindByEmail = restTemplate.getForEntity(url, String.class);

		String body = responseFromFindByEmail.getBody();
		int usersFoundSize = 0;
		Iterator<JsonNode> usersfoundIterator;
		try {
			usersfoundIterator = new ObjectMapper().readTree(body).iterator();
			while (usersfoundIterator.hasNext()) {
				usersFoundSize++;
				JsonNode usersFound = usersfoundIterator.next();
				assertEquals(userDTOAdded.getEmail(), usersFound.get("email").textValue());
			}
		} catch (IOException e) {
			fail("error getting list of drivers from json");
		}

		assertEquals(HttpStatus.OK, responseFromFindByEmail.getStatusCode());
		assertEquals(2, usersFoundSize);

	}

	@Test
	public void shouldAddAndDeleteUserById() {
		// Setup
		UserDTO newUser = new UserDTO();
		newUser.setName("tulio");
		newUser.setEmail("tuliotanikawa@gmail.com");
		newUser.setPassword("123");
		newUser.setPhone("99999999");
		newUser.setSurname("tanikawa");
		newUser.setUsername("teste");
		newUser.setIs_enabled(true);

		HttpEntity<UserDTO> entity = new HttpEntity<UserDTO>(newUser, headers);

		ResponseEntity<UserDTO> response = restTemplate.exchange("/api/users/", HttpMethod.POST, entity, UserDTO.class);

		UserDTO userDTOAdded = response.getBody();

		String urlDelete = String.format("/api/users/%d", userDTOAdded.getId());
		ResponseEntity<String> responseFromDelete = restTemplate.exchange(urlDelete, HttpMethod.DELETE, null, String.class);

		HttpStatus responseContent = responseFromDelete.getStatusCode();
		assertEquals(HttpStatus.OK, responseContent);
	}

	@Test
	public void shouldGetHTTPNotFoundStatusTryingToFindByNonExistentId() {
		String url = String.format("/api/users/%d", 111111111);
		ResponseEntity<UserDTO> responseFromFindById = restTemplate.getForEntity(url, UserDTO.class);
		assertEquals(HttpStatus.NOT_FOUND, responseFromFindById.getStatusCode());

	}

}