# backend_users_java

Backend of a Rest API for an "Users" CRUD


To install it, open your favorite IDE, it will probably allow you to import a Maven project. Either download the zip or clone this rep into a folder, import Maven project from your IDE, update Maven project so it resolves all dependencies (It will do it automatically, probably, just make sure your Proxy setup is configured correctly if behind one).

After all dependencies are resolved, just run as a java application.

//TODO put swagger to build the rest documentation;